<h2>Capture My Attendance</h2>


@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<h3>{{$first_name}}</h3>
@if(isset($attendance->id))
<form action="/attendance/update/{{$attendance->id}}" method="post">
    @method('patch')
@else
<form action="/attendance/save" method="post">
@endif
    @csrf
<table>
    <tr>
        <td>Tarikh</td>
        <td>:</td>
        <td>{{date('d-m-Y H:i:s')}}</td>
    </tr>
    <tr>
        <td>Koordinat</td>
        <td>:</td>
        <td>3.1621587,101.582908</td>
    </tr>
    <tr>
        <td>Lampiran</td>
        <td>:</td>
        <td><input type="file" name="attachment" id="attachment"></td>
    </tr>
    <tr>
        <td>Catatan <span style="color:red">*</span></td>
        <td>:</td>
        <td>
            <textarea name="remarks" id="remarks" cols="30" rows="10">{{$attendance->remarks}}</textarea>
        </td>
    </tr>
    <tr>
        <td>
            <input type="submit" value="Punch In">
            <a href="/attendance/list"><button type="button">Kembali</button></a>
        </td>
    </tr>
</table>
</form>
