@extends('layouts.admin')
@section('title', 'Senarai Kehadiran')
@section('content')
<h2>Senarai Kehadiran</h2>
<form action="/attendance/list" class="col-6">
    @csrf
<div class="mb-3 row">
    <label for="date_in" class="col-sm-2 col-form-label">Tarikh</label>
    <input type="date" class="form-control" name="date_in" id="date_in" value="{{$date_in}}">
  </div>

  <div class="mb-3 row">
    <label for="remarks" class="col-sm-2 col-form-label">Catatan</label>
    <input type="text" class="form-control" name="remarks" id="remarks" value="{{$remarks}}">
  </div>

  
    <input type="submit" value="Teruskan" class="btn btn-primary">
    <a href="/attendance/list" class="btn btn-danger">Set Semula</a>
    
</form>

<a href="/attendance/capture" class="btn btn-success">New Attendance</a>

<table class="table table-striped">
    <tr>
        <th>Bil</th>
        <th>Tarikh</th>
        <th>Masa</th>
        <th>Catatan</th>
        <th>Tindakan</th>
    </tr>
    @php 
    $no = $attendances->firstItem();
    @endphp
    @foreach($attendances as $attendance)
    <tr>
        <td>{{$no++}}</td>
        <td>{{date('d-m-Y', strtotime($attendance->date_in))}}</td>
        <td>{{date('h:i A',strtotime($attendance->date_in))}}</td>
        <!-- <td>{{$attendance->remarks}}</td> -->
        <td>{!! $attendance->remarks !!}</td>
        
        <td>

            
            <form action="/attendance/delete/{{$attendance->id}}" method="POST">
                @csrf
                @method('DELETE')
                <a href="/attendance/edit/{{$attendance->id}}" class="btn btn-secondary">Kemaskini</a>
                <input type="submit" onclick="return confirm('Anda Pasti')" class="btn btn-danger" value="Hapus">
            </form>
            

        </td>
    </tr>
    @endforeach

</table>
@php 
//var_dump(request()); 
@endphp
{{ $attendances->appends(['remarks'=>$remarks,'date_in'=>$date_in])->links() }}
@endsection