<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BscController;
use App\Http\Controllers\AttendanceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('/balance', BscController::class);

Route::prefix('attendance')->group(function () {
    Route::get('/list', 
        [AttendanceController::class,'list'])->name('list.attendance');
    Route::get('/capture/{userid?}', 
        [AttendanceController::class,'capture'])->name('capture.attendance');
    Route::get('/edit/{id}', 
        [AttendanceController::class,'edit'])->name('edit.attendance');
    Route::post('/save', 
        [AttendanceController::class,'save'])->name('save.attendance');
    Route::patch('/update/{id}', 
        [AttendanceController::class,'update'])->name('update.attendance');
    Route::delete('/delete/{id}', 
        [AttendanceController::class,'delete'])->name('delete.attendance');
});



// Route::get('/capture-attendance/{userid?}', function ($user='') {
//     // echo 'Capture attendance code here '.$user;
//     // return view('attendance.capture_attendance',['user'=>$user]);
//     $profile = [];
//     $organization = [];
//     $data['user'] = $user;
//     $data['profile'] = $profile;
//     $data['organization'] = $organization;
//     // return view('attendance.capture_attendance',compact('user','profile','organization'));
//     return view('attendance.capture_attendance',$data);
// })->name('capture.attendance');



// Route::post('/save-attendance/{userid?}', function ($user='') {
//     echo 'Save attendance code here '.$user;
// })->name('save.attendance');

Route::get('/dashboard-attendance', function () {
    echo 'Dashboard attendance code here';
})->name('dashboard.attendance');