<?php

namespace App\Http\Controllers;

use App\Models\Attendance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AttendanceController extends Controller
{
    public function list(Request $req){
        $date_in = $req->date_in; 
        $remarks = $req->remarks; 
        
        /*
        $attendances = Attendance::select('*');

        if (isset($req->date_in)){
            $attendances = $attendances->where(DB::raw("DATE_FORMAT(date_in,'%Y-%m-%d')"),$req->date_in);            
        }

        if (isset($req->remarks)){
            $attendances = $attendances->where('remarks','LIKE','%'.$req->remarks.'%');
        }

        $attendances = $attendances->orderBy('date_in')->paginate(2);
        */
        //--------------

        $attendances = Attendance::where(function($q) use ($remarks,$date_in){
            if (isset($date_in)){
                $q = $q->where(DB::raw("DATE_FORMAT(date_in,'%Y-%m-%d')"),$date_in);
            }
    
            if (isset($remarks)){
                $q = $q->where('remarks','LIKE','%'.$remarks.'%');
            }   
        })->orderBy('date_in')->paginate(2);

        return view('attendance.list',compact('attendances','date_in','remarks'));
    }

    public function capture($user=''){
        $attendance = new Attendance();
        $data['first_name'] = 'Raj';
        $data['attendance'] = $attendance;
        return view('attendance.capture_attendance',$data);
    }

    public function save(Request $req){

        $validated = $req->validate([
            'remarks' => 'required'
        ]);

        $attendance = new Attendance();
        $attendance->remarks = $req->remarks;
        $attendance->date_in = date('Y-m-d H:i');//$req->date_in;
        $attendance->user_id = 1;//$req->user_id;
        $attendance->save();
        // return route('list.attendance');
        return redirect('/attendance/list');
    }

    public function edit($id){
        $attendance = Attendance::find($id);
        $data['first_name'] = 'Raj';
        $data['attendance'] = $attendance;
        return view('attendance.capture_attendance',$data);
    }

    public function update($id, Request $req){
        $attendance = Attendance::find($id);
        $attendance->remarks = $req->remarks;
        $attendance->date_in = date('Y-m-d H:i');//$req->date_in;
        $attendance->user_id = 1;//$req->user_id;
        $attendance->save();
        return redirect('/attendance/list');
    }
    
    public function delete($id){
        Attendance::find($id)->delete();
        return redirect('/attendance/list');
    }

    


    public function view(){
        
    }

    

    

    public function print(){
        
    }

    public function approve(){
        
    }
}
